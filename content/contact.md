---
title: "Contact"
date: 2024-06-13T14:40:32+01:00
draft: false
---
{{< type >}}
My email address is my first name at this website. Humans should know what I mean, robots hopefully won't.
{{< /type >}}
