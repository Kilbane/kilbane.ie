---
title: "About"
date: 2023-07-14T17:09:11+01:00
draft: false
---
I am Neil Kilbane. In 2023 I graduated with a Master's in Computer Science from
Trinity College Dublin.

# Skills

I don't claim to be an expert in any of these, but I know enough to get by. I'm
always trying to improve my skillset, either by learning new tools and
languages, or by diving deeper into the ones I already know.

## Development
- General Purpose Programming: Java, C, C++, Ruby, Python, Go, Haskell, Lua
- Logic Programming: Prolog
- Scripting: Shell, Bash, AWK
- Assembly: ARM, x86
- Compiler Design: Lex, Yacc
- Web Development: HTML, CSS, JavaScript, Hugo, NGINX, Vue, React, Node.js, Vitest, Cypress
- Databases: SQLite, PostgreSQL, MariaDB, Firebase
- UI Design: Figma
- Management: Agile, SCRUM, Git, Jira

## Operations
- Operating Systems: Linux
- Containerisation: Docker, Kubernetes
- Network Traffic Analysis: tcpdump, Wireshark
- Cloud: Hetzner, AWS, Cloudflare

## Other computer stuff
- Microprocessor Design: Verilog, VHDL
- Formal Verification: Promela, Spin, Dafny
- Typesetting: groff, LaTeX
- Graphic Design: Inkscape, GIMP
- Data Visualisation: Processing, D3
- Numerical Analysis: Octave, MATLAB
