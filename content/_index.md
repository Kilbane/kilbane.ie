---
title: "Home"
date: 2023-07-13T19:48:27+01:00
draft: false
---
{{< type >}}
    Hello, I'm Neil Kilbane, and this is my website. I started it when I was in college but I never had much time to do anything interesting with it. Now that my academic days are behind me, I want to share a wider variety of content here.
{{< break >}}
{{< break >}}
    Have a poke around. You might find something that captures your attention.
{{< /type >}}

