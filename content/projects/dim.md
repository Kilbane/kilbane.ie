---
title: "Dim"
date: 2024-04-23T21:40:45+01:00
draft: false
code: "https://gitlab.com/kilbane/dim"
---
Dim is a data inventory manager. This program will help you keep track of the
data that you submit to different services.
