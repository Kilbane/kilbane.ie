---
title: "Statpad"
date: 2024-04-24T23:46:37+01:00
draft: false
code: "https://gitlab.com/kilbane/statpad"
---
This is a program for tracking the stats for a basketball game.

## Usage

1. Run java Tracker.
2. Enter <number> <event>, until you want to quit.
3. To quit, enter q.
