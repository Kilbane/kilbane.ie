---
title: "Duelist"
date: 2023-08-22T23:07:31+01:00
draft: false
code: "https://gitlab.com/kilbane/duelist"
---
Duelist is my personal to-do application. Its name is a concatenation of the words due and list.
Duelist is written in Go and uses a PostgreSQL database. It is a work in progress.

## Features
- Creating tasks
- Listing tasks
- Updating tasks
- Deleting tasks

## Planned Features
- Associating tasks with shell scripts that open relevant files
- Others that I haven't thought of yet
