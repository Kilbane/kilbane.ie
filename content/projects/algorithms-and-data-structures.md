---
title: "Algorithms and Data Structures"
date: 2023-08-21T12:35:47+01:00
draft: false
code: "https://gitlab.com/kilbane/algorithms-and-data-structures"
---
Coursework for the Year 2 module Algorithms and Data Structures taught by Dr.
Vasileios Koutavas and Dr. Ivana Dusparic.

## Arith
This exercise involved writing methods for validating, evaluating and
converting between prefix and postfix arithmetic expressions.

## BST
This exercise involved writing methods for a binary search tree.

## Collinear
This exercise involved writing methods for discovering collinear points from
different arrays.

## DoublyLinkedList
This exercise involved writing methods for a doubly linked list.

## Competition
This exercise involved solving a shortest-path problem using Dijkstra's
algorithm and the Floyd-Warshall algorithm.

## SortComparison
This exercise involved implementing different sorting algorithms and comparing
their performance for different sized inputs.
