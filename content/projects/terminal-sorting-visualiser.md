---
title: "Terminal Sorting Visualiser"
date: 2023-07-13T19:29:40+01:00
draft: false
code: "https://gitlab.com/kilbane/terminal-sorting-visualiser"
---
The sorting algorithm visualiser is a fairly common side project, but I've
never seen anyone make a terminal-based one. I used C and the ncurses library
to do just that.

## To build:
```
make
```

## To run:
```
./main
```

## Usage:
Select the algorithm you want from the menu. Hold down any key to make the algorithm progress.
